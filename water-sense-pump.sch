EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2250 2500 0    50   Input ~ 0
water_sense_a
Text HLabel 2300 2950 0    50   Input ~ 0
water_sense_b
$Comp
L Device:R R10
U 1 1 5F535034
P 3050 3000
F 0 "R10" H 2980 2954 50  0000 R CNN
F 1 "100k" H 2980 3045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2980 3000 50  0001 C CNN
F 3 "~" H 3050 3000 50  0001 C CNN
F 4 "RR08P100KDCT-ND" H 3050 3000 50  0001 C CNN "Partnum"
	1    3050 3000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5F53AEAE
P 2400 3100
F 0 "#PWR0124" H 2400 2850 50  0001 C CNN
F 1 "GND" H 2405 2927 50  0000 C CNN
F 2 "" H 2400 3100 50  0001 C CNN
F 3 "" H 2400 3100 50  0001 C CNN
	1    2400 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2950 2400 2950
Wire Wire Line
	2400 2950 2400 3100
$Comp
L Transistor_BJT:2N3906 Q2
U 1 1 5F53BD22
P 2950 2500
F 0 "Q2" H 3140 2454 50  0000 L CNN
F 1 "2N3906" H 3140 2545 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3150 2425 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3906.pdf" H 2950 2500 50  0001 L CNN
	1    2950 2500
	1    0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5F5401EE
P 2500 2500
F 0 "R7" V 2707 2500 50  0000 C CNN
F 1 "1k" V 2616 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2430 2500 50  0001 C CNN
F 3 "~" H 2500 2500 50  0001 C CNN
F 4 "311-1.0KGRCT-ND" H 2500 2500 50  0001 C CNN "Partnum"
	1    2500 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2650 2500 2700 2500
Wire Wire Line
	2350 2500 2250 2500
Wire Wire Line
	3050 1950 3050 2050
$Comp
L power:GND #PWR0125
U 1 1 5F545F46
P 3050 3250
F 0 "#PWR0125" H 3050 3000 50  0001 C CNN
F 1 "GND" H 3055 3077 50  0000 C CNN
F 2 "" H 3050 3250 50  0001 C CNN
F 3 "" H 3050 3250 50  0001 C CNN
	1    3050 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3150 3050 3250
$Comp
L Device:R R11
U 1 1 5F549699
P 3650 2800
F 0 "R11" V 3857 2800 50  0000 C CNN
F 1 "1k" V 3766 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3580 2800 50  0001 C CNN
F 3 "~" H 3650 2800 50  0001 C CNN
F 4 "311-1.0KGRCT-ND" H 3650 2800 50  0001 C CNN "Partnum"
	1    3650 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 2700 3050 2800
Wire Wire Line
	3500 2800 3350 2800
Connection ~ 3050 2800
Wire Wire Line
	3050 2800 3050 2850
$Comp
L power:+12V #PWR0126
U 1 1 5F54E4D6
P 5850 3500
F 0 "#PWR0126" H 5850 3350 50  0001 C CNN
F 1 "+12V" H 5865 3673 50  0000 C CNN
F 2 "" H 5850 3500 50  0001 C CNN
F 3 "" H 5850 3500 50  0001 C CNN
	1    5850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2800 4200 2800
Connection ~ 4200 2800
Text HLabel 2300 3750 0    50   Input ~ 0
motor_cut_off
Wire Wire Line
	2300 3750 3450 3750
Wire Wire Line
	3450 3750 3450 3250
$Comp
L Device:R R12
U 1 1 5F562FE7
P 3650 3250
F 0 "R12" V 3857 3250 50  0000 C CNN
F 1 "1k" V 3766 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3580 3250 50  0001 C CNN
F 3 "~" H 3650 3250 50  0001 C CNN
F 4 "311-1.0KGRCT-ND" H 3650 3250 50  0001 C CNN "Partnum"
	1    3650 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3450 3250 3500 3250
$Comp
L power:GND #PWR0127
U 1 1 5F55F8BE
P 4200 3850
F 0 "#PWR0127" H 4200 3600 50  0001 C CNN
F 1 "GND" H 4205 3677 50  0000 C CNN
F 2 "" H 4200 3850 50  0001 C CNN
F 3 "" H 4200 3850 50  0001 C CNN
	1    4200 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2800 4200 3050
Wire Wire Line
	3800 3250 3850 3250
Wire Wire Line
	4200 3850 4200 3650
$Comp
L Transistor_BJT:2N3904 Q3
U 1 1 5F52E8C8
P 4100 3250
F 0 "Q3" H 4290 3296 50  0000 L CNN
F 1 "2N3904" H 4290 3205 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4300 3175 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 4100 3250 50  0001 L CNN
	1    4100 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5F5676F5
P 3850 3500
F 0 "R13" H 3780 3454 50  0000 R CNN
F 1 "100k" H 3780 3545 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3780 3500 50  0001 C CNN
F 3 "~" H 3850 3500 50  0001 C CNN
F 4 "RR08P100KDCT-ND" H 3850 3500 50  0001 C CNN "Partnum"
	1    3850 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 3650 4200 3650
Connection ~ 4200 3650
Wire Wire Line
	4200 3650 4200 3450
Wire Wire Line
	3850 3350 3850 3250
Connection ~ 3850 3250
Wire Wire Line
	3850 3250 3900 3250
Text HLabel 2300 3550 0    50   Input ~ 0
water_sense_out
$Comp
L Device:R R9
U 1 1 5F56D4C5
P 2700 3550
F 0 "R9" V 2907 3550 50  0000 C CNN
F 1 "1k" V 2816 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2630 3550 50  0001 C CNN
F 3 "~" H 2700 3550 50  0001 C CNN
F 4 "311-1.0KGRCT-ND" H 2700 3550 50  0001 C CNN "Partnum"
	1    2700 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 3550 2550 3550
Wire Wire Line
	2850 3550 3350 3550
Wire Wire Line
	3350 3550 3350 2800
Connection ~ 3350 2800
Wire Wire Line
	3350 2800 3050 2800
$Comp
L Connector_Generic:Conn_01x02 J14
U 1 1 5F56EE9E
P 6150 3600
F 0 "J14" H 6230 3592 50  0000 L CNN
F 1 "Conn_01x02" H 6230 3501 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 6150 3600 50  0001 C CNN
F 3 "~" H 6150 3600 50  0001 C CNN
	1    6150 3600
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP120 Q4
U 1 1 5F52E238
P 4700 2800
F 0 "Q4" H 4907 2846 50  0000 L CNN
F 1 "TIP120" H 4907 2755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4900 2725 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 4700 2800 50  0001 L CNN
	1    4700 2800
	1    0    0    -1  
$EndComp
Text Label 4800 2500 1    50   ~ 0
pump_out
Text Label 5850 3700 2    50   ~ 0
pump_out
Wire Wire Line
	5950 3700 5850 3700
Wire Wire Line
	4800 2500 4800 2550
Wire Wire Line
	5850 3500 5850 3600
Wire Wire Line
	5850 3600 5950 3600
$Comp
L power:+3.3V #PWR0128
U 1 1 5F55C537
P 3050 1950
F 0 "#PWR0128" H 3050 1800 50  0001 C CNN
F 1 "+3.3V" H 3065 2123 50  0000 C CNN
F 2 "" H 3050 1950 50  0001 C CNN
F 3 "" H 3050 1950 50  0001 C CNN
	1    3050 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5F55DB0A
P 4800 3400
F 0 "#PWR0129" H 4800 3150 50  0001 C CNN
F 1 "GND" H 4805 3227 50  0000 C CNN
F 2 "" H 4800 3400 50  0001 C CNN
F 3 "" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5F55FFFE
P 2700 2250
F 0 "R8" H 2630 2204 50  0000 R CNN
F 1 "100k" H 2630 2295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2630 2250 50  0001 C CNN
F 3 "~" H 2700 2250 50  0001 C CNN
F 4 "RR08P100KDCT-ND" H 2700 2250 50  0001 C CNN "Partnum"
	1    2700 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 2100 2700 2050
Wire Wire Line
	2700 2050 3050 2050
Connection ~ 3050 2050
Wire Wire Line
	3050 2050 3050 2300
Wire Wire Line
	2700 2400 2700 2500
Connection ~ 2700 2500
Wire Wire Line
	2700 2500 2750 2500
Wire Wire Line
	4800 3400 4800 3150
Wire Wire Line
	4200 2800 4500 2800
$Comp
L Device:C C6
U 1 1 5F6BA4C1
P 5100 2550
F 0 "C6" V 4848 2550 50  0000 C CNN
F 1 ".1u" V 4939 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5138 2400 50  0001 C CNN
F 3 "~" H 5100 2550 50  0001 C CNN
F 4 "399-1099-1-ND" H 5100 2550 50  0001 C CNN "Partnum"
	1    5100 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 2550 4800 2550
Connection ~ 4800 2550
Wire Wire Line
	4800 2550 4800 2600
Wire Wire Line
	5250 2550 5250 3150
Wire Wire Line
	5250 3150 4800 3150
Connection ~ 4800 3150
Wire Wire Line
	4800 3150 4800 3000
$EndSCHEMATC
