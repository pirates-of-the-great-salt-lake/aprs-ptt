EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4750 5250 1150 200 
U 5F4AFF1A
F0 "btech-aprs" 50
F1 "btech-aprrs.sch" 50
F2 "ptt" I R 5900 5350 50 
$EndSheet
$Comp
L Connector_Generic:Conn_02x23_Odd_Even J9
U 1 1 5F526232
P 9000 4150
F 0 "J9" H 9050 5467 50  0000 C CNN
F 1 "Conn_02x23_Odd_Even" H 9050 5376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x23_P2.54mm_Vertical" H 9000 4150 50  0001 C CNN
F 3 "~" H 9000 4150 50  0001 C CNN
	1    9000 4150
	1    0    0    -1  
$EndComp
$Sheet
S 4750 5700 1150 500 
U 5F52DD5E
F0 "water-sense-pump" 50
F1 "water-sense-pump.sch" 50
F2 "water_sense_a" I L 4750 5800 50 
F3 "water_sense_b" I L 4750 5900 50 
F4 "motor_cut_off" I R 5900 6000 50 
F5 "water_sense_out" I R 5900 6100 50 
$EndSheet
$Sheet
S 4750 4200 1150 200 
U 5F57EF45
F0 "nav-lights" 50
F1 "nav_lights.sch" 50
F2 "nav_light" I R 5900 4300 50 
$EndSheet
$Sheet
S 4750 4650 1150 300 
U 5F58486F
F0 "serial_conv" 50
F1 "serial_conv.sch" 50
F2 "15v_tx" I L 4750 4750 50 
F3 "15v_rx" I L 4750 4850 50 
F4 "3.3v_tx" I R 5900 4750 50 
F5 "3.3v_rx" I R 5900 4850 50 
$EndSheet
$Sheet
S 4750 6500 1150 400 
U 5F59B247
F0 "radio-power-supply" 50
F1 "radio-power-supply.sch" 50
F2 "radio_power" I L 4750 6750 50 
$EndSheet
Text Notes 2300 5700 0    50   ~ 0
Magnetometer trigger gpio - 5 pins\nWater sensor gpio input\nMotor disable gpio\nNav light gpio input\nHeartbeat GPIO\nCharge Controller -- UART1 TXD & RXD\nMotorS PWM tbd\nMotorP PWM tbd\nRudder PWM tbd\nReset line from raspberry pi -- gpio pin 10 p9
$Comp
L Connector_Generic:Conn_01x04 J13
U 1 1 5F543F6F
P 1100 6350
F 0 "J13" H 1180 6342 50  0000 L CNN
F 1 "RPI " H 1180 6251 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B4B-EH-A_1x04_P2.50mm_Vertical" H 1100 6350 50  0001 C CNN
F 3 "~" H 1100 6350 50  0001 C CNN
	1    1100 6350
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x05 J12
U 1 1 5F544BC0
P 1100 5550
F 0 "J12" H 1180 5592 50  0000 L CNN
F 1 "Mag " H 1180 5501 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B5B-EH-A_1x05_P2.50mm_Vertical" H 1100 5550 50  0001 C CNN
F 3 "~" H 1100 5550 50  0001 C CNN
	1    1100 5550
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J11
U 1 1 5F545FE0
P 1100 4800
F 0 "J11" H 1180 4792 50  0000 L CNN
F 1 "charge_cont" H 1180 4701 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B4B-EH-A_1x04_P2.50mm_Vertical" H 1100 4800 50  0001 C CNN
F 3 "~" H 1100 4800 50  0001 C CNN
	1    1100 4800
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 5F5478C5
P 1100 3750
F 0 "J8" H 1200 3750 50  0000 L CNN
F 1 "12v terminal" H 1200 3650 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1100 3750 50  0001 C CNN
F 3 "~" H 1100 3750 50  0001 C CNN
	1    1100 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J5
U 1 1 5F549A9E
P 1100 1650
F 0 "J5" H 1128 1626 50  0000 L CNN
F 1 "6dof" H 1128 1535 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 1100 1650 50  0001 C CNN
F 3 "~" H 1100 1650 50  0001 C CNN
	1    1100 1650
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0105
U 1 1 5F56DC3A
P 1400 1300
F 0 "#PWR0105" H 1400 1150 50  0001 C CNN
F 1 "+3.3V" H 1415 1473 50  0000 C CNN
F 2 "" H 1400 1300 50  0001 C CNN
F 3 "" H 1400 1300 50  0001 C CNN
	1    1400 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1350 1400 1350
Wire Wire Line
	1400 1350 1400 1300
$Comp
L power:GND #PWR0106
U 1 1 5F56E425
P 2000 1450
F 0 "#PWR0106" H 2000 1200 50  0001 C CNN
F 1 "GND" H 2005 1277 50  0000 C CNN
F 2 "" H 2000 1450 50  0001 C CNN
F 3 "" H 2000 1450 50  0001 C CNN
	1    2000 1450
	1    0    0    -1  
$EndComp
Text Label 1400 1550 0    50   ~ 0
i2c_scl
Wire Wire Line
	1300 1450 1850 1450
Wire Wire Line
	1300 1550 1400 1550
Text Label 1400 1650 0    50   ~ 0
i2c_sda
Wire Wire Line
	1400 1650 1300 1650
$Comp
L Device:R R6
U 1 1 5F5708E9
P 1850 1750
F 0 "R6" H 1780 1704 50  0000 R CNN
F 1 "1k" H 1780 1795 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1780 1750 50  0001 C CNN
F 3 "~" H 1850 1750 50  0001 C CNN
F 4 "311-1.0KGRCT-ND" H 1850 1750 50  0001 C CNN "Partnum"
	1    1850 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 1950 1850 1900
Wire Wire Line
	1300 1950 1850 1950
Wire Wire Line
	1850 1600 1850 1450
Connection ~ 1850 1450
Wire Wire Line
	1850 1450 2000 1450
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5F54A6EF
P 1100 2800
F 0 "J6" H 1180 2792 50  0000 L CNN
F 1 "Water Sense" H 1180 2701 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1100 2800 50  0001 C CNN
F 3 "~" H 1100 2800 50  0001 C CNN
	1    1100 2800
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0107
U 1 1 5F5987D8
P 1300 3650
F 0 "#PWR0107" H 1300 3500 50  0001 C CNN
F 1 "+12V" H 1315 3823 50  0000 C CNN
F 2 "" H 1300 3650 50  0001 C CNN
F 3 "" H 1300 3650 50  0001 C CNN
	1    1300 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F59CD7D
P 1300 3200
F 0 "#PWR0108" H 1300 2950 50  0001 C CNN
F 1 "GND" H 1305 3027 50  0000 C CNN
F 2 "" H 1300 3200 50  0001 C CNN
F 3 "" H 1300 3200 50  0001 C CNN
	1    1300 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5F59E8E3
P 1300 3750
F 0 "#PWR0109" H 1300 3500 50  0001 C CNN
F 1 "GND" H 1305 3577 50  0000 C CNN
F 2 "" H 1300 3750 50  0001 C CNN
F 3 "" H 1300 3750 50  0001 C CNN
	1    1300 3750
	1    0    0    -1  
$EndComp
Text Label 4550 6750 2    50   ~ 0
radio_power
Wire Wire Line
	4750 6750 4550 6750
Text Label 1300 4100 0    50   ~ 0
radio_power
Text Label 4600 5800 2    50   ~ 0
water_sense_a
Wire Wire Line
	4600 5800 4750 5800
Text Label 4600 5900 2    50   ~ 0
water_sense_b
Wire Wire Line
	4600 5900 4750 5900
Text Label 6050 6100 0    50   ~ 0
water_sense
Text Label 6050 6000 0    50   ~ 0
water_pump_cutoff
Wire Wire Line
	5900 6100 6050 6100
Wire Wire Line
	6050 6000 5900 6000
Text Label 6000 5350 0    50   ~ 0
ptt
Wire Wire Line
	5900 5350 6000 5350
Text Notes 650  1950 0    50   ~ 0
+3.3\ngnd\nscl\nsda\nxda\nxcl\nad0\nint
$Comp
L power:GND #PWR0110
U 1 1 5F5A8A18
P 1300 4200
F 0 "#PWR0110" H 1300 3950 50  0001 C CNN
F 1 "GND" H 1305 4027 50  0000 C CNN
F 2 "" H 1300 4200 50  0001 C CNN
F 3 "" H 1300 4200 50  0001 C CNN
	1    1300 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J10
U 1 1 5F5A7E35
P 1100 4200
F 0 "J10" H 1180 4192 50  0000 L CNN
F 1 "Radio_Power" H 1180 4101 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1100 4200 50  0001 C CNN
F 3 "~" H 1100 4200 50  0001 C CNN
	1    1100 4200
	-1   0    0    1   
$EndComp
Text Notes 600  4750 0    50   ~ 0
+15v\ngnd\ntx\nrx
$Comp
L power:+15V #PWR0111
U 1 1 5F5F2382
P 1300 4700
F 0 "#PWR0111" H 1300 4550 50  0001 C CNN
F 1 "+15V" H 1315 4873 50  0000 C CNN
F 2 "" H 1300 4700 50  0001 C CNN
F 3 "" H 1300 4700 50  0001 C CNN
	1    1300 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5F5F3D12
P 1800 4800
F 0 "#PWR0112" H 1800 4550 50  0001 C CNN
F 1 "GND" H 1805 4627 50  0000 C CNN
F 2 "" H 1800 4800 50  0001 C CNN
F 3 "" H 1800 4800 50  0001 C CNN
	1    1800 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 4800 1800 4800
Text Label 1300 4900 0    50   ~ 0
charge_tx
Text Label 1300 5000 0    50   ~ 0
charge_rx
Text Notes 600  5650 0    50   ~ 0
+3.3.v \nGnd \nSCL \nSDA \nTrig
$Comp
L power:+3.3V #PWR0113
U 1 1 5F5FBDD0
P 1300 5350
F 0 "#PWR0113" H 1300 5200 50  0001 C CNN
F 1 "+3.3V" H 1315 5523 50  0000 C CNN
F 2 "" H 1300 5350 50  0001 C CNN
F 3 "" H 1300 5350 50  0001 C CNN
	1    1300 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5F5FC730
P 1800 5450
F 0 "#PWR0114" H 1800 5200 50  0001 C CNN
F 1 "GND" H 1805 5277 50  0000 C CNN
F 2 "" H 1800 5450 50  0001 C CNN
F 3 "" H 1800 5450 50  0001 C CNN
	1    1800 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5450 1800 5450
Text Label 1400 5550 0    50   ~ 0
i2c_scl
Text Label 1400 5650 0    50   ~ 0
i2c_sda
Wire Wire Line
	1300 5550 1400 5550
Wire Wire Line
	1300 5650 1400 5650
Text Label 1400 5750 0    50   ~ 0
mag_trigger
Wire Wire Line
	1300 5750 1400 5750
Text Label 4600 4750 2    50   ~ 0
charge_tx
Text Label 4600 4850 2    50   ~ 0
charge_rx
Wire Wire Line
	4600 4750 4750 4750
Wire Wire Line
	4750 4850 4600 4850
Text Label 6000 4300 0    50   ~ 0
nav_light
Wire Wire Line
	5900 4300 6000 4300
Text Label 1400 2800 0    50   ~ 0
water_sense_a
Text Label 1400 2700 0    50   ~ 0
water_sense_b
Wire Wire Line
	1300 2700 1400 2700
Wire Wire Line
	1300 2800 1400 2800
Text Notes 550  6500 0    50   ~ 0
+5V\nGND\nHbt \nBBRst
$Comp
L power:+5V #PWR0115
U 1 1 5F5983C2
P 1300 3100
F 0 "#PWR0115" H 1300 2950 50  0001 C CNN
F 1 "+5V" H 1315 3273 50  0000 C CNN
F 2 "" H 1300 3100 50  0001 C CNN
F 3 "" H 1300 3100 50  0001 C CNN
	1    1300 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J7
U 1 1 5F548DD7
P 1100 3200
F 0 "J7" H 1200 3200 50  0000 L CNN
F 1 "5v Terminal" H 1200 3100 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1100 3200 50  0001 C CNN
F 3 "~" H 1100 3200 50  0001 C CNN
	1    1100 3200
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 5F619A71
P 1300 6250
F 0 "#PWR0116" H 1300 6100 50  0001 C CNN
F 1 "+5V" H 1315 6423 50  0000 C CNN
F 2 "" H 1300 6250 50  0001 C CNN
F 3 "" H 1300 6250 50  0001 C CNN
	1    1300 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5F61A740
P 1700 6350
F 0 "#PWR0117" H 1700 6100 50  0001 C CNN
F 1 "GND" H 1705 6177 50  0000 C CNN
F 2 "" H 1700 6350 50  0001 C CNN
F 3 "" H 1700 6350 50  0001 C CNN
	1    1700 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6350 1700 6350
Text Label 1400 6450 0    50   ~ 0
hbt
Wire Wire Line
	1300 6450 1400 6450
Text Label 1400 6550 0    50   ~ 0
bbrst
Wire Wire Line
	1300 6550 1400 6550
Text Label 9400 3450 0    50   ~ 0
bbrst
Wire Wire Line
	9300 3450 9400 3450
$Comp
L power:GND #PWR0118
U 1 1 5F623DC4
P 8750 5350
F 0 "#PWR0118" H 8750 5100 50  0001 C CNN
F 1 "GND" H 8755 5177 50  0000 C CNN
F 2 "" H 8750 5350 50  0001 C CNN
F 3 "" H 8750 5350 50  0001 C CNN
	1    8750 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5F624A2E
P 9350 5350
F 0 "#PWR0119" H 9350 5100 50  0001 C CNN
F 1 "GND" H 9355 5177 50  0000 C CNN
F 2 "" H 9350 5350 50  0001 C CNN
F 3 "" H 9350 5350 50  0001 C CNN
	1    9350 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5150 9350 5150
Wire Wire Line
	9350 5150 9350 5250
Wire Wire Line
	9300 5250 9350 5250
Connection ~ 9350 5250
Wire Wire Line
	9350 5250 9350 5350
Wire Wire Line
	8800 5250 8750 5250
Wire Wire Line
	8750 5250 8750 5350
Wire Wire Line
	8750 5250 8750 5150
Wire Wire Line
	8750 5150 8800 5150
Connection ~ 8750 5250
$Comp
L power:GND #PWR0120
U 1 1 5F6275AB
P 8450 3050
F 0 "#PWR0120" H 8450 2800 50  0001 C CNN
F 1 "GND" H 8455 2877 50  0000 C CNN
F 2 "" H 8450 3050 50  0001 C CNN
F 3 "" H 8450 3050 50  0001 C CNN
	1    8450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3050 8800 3050
$Comp
L power:GND #PWR0121
U 1 1 5F629504
P 9650 3050
F 0 "#PWR0121" H 9650 2800 50  0001 C CNN
F 1 "GND" H 9655 2877 50  0000 C CNN
F 2 "" H 9650 3050 50  0001 C CNN
F 3 "" H 9650 3050 50  0001 C CNN
	1    9650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 3050 9650 3050
$Comp
L power:+5V #PWR0122
U 1 1 5F62B1A8
P 9050 2350
F 0 "#PWR0122" H 9050 2200 50  0001 C CNN
F 1 "+5V" H 9065 2523 50  0000 C CNN
F 2 "" H 9050 2350 50  0001 C CNN
F 3 "" H 9050 2350 50  0001 C CNN
	1    9050 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 2350 9050 2350
Wire Wire Line
	9050 2350 9400 2350
Wire Wire Line
	9400 2350 9400 3250
Wire Wire Line
	9400 3250 9300 3250
Connection ~ 9050 2350
Wire Wire Line
	8700 3250 8800 3250
Wire Wire Line
	8700 2350 8700 3250
$Comp
L power:+3.3V #PWR0123
U 1 1 5F6319CC
P 9050 2650
F 0 "#PWR0123" H 9050 2500 50  0001 C CNN
F 1 "+3.3V" H 9065 2823 50  0000 C CNN
F 2 "" H 9050 2650 50  0001 C CNN
F 3 "" H 9050 2650 50  0001 C CNN
	1    9050 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 2650 8750 2650
Wire Wire Line
	8750 2650 8750 3150
Wire Wire Line
	8750 3150 8800 3150
Wire Wire Line
	9300 3150 9350 3150
Wire Wire Line
	9350 3150 9350 2650
Wire Wire Line
	9350 2650 9050 2650
Connection ~ 9050 2650
Text Label 9400 4150 0    50   ~ 0
bb_charge_tx
Text Label 9400 4250 0    50   ~ 0
bb_charge_rx
Wire Wire Line
	9400 4250 9300 4250
Wire Wire Line
	9300 4150 9400 4150
Text Label 9400 4050 0    50   ~ 0
pwm_0a
Wire Wire Line
	9300 4050 9400 4050
Text Label 8700 4050 2    50   ~ 0
pwm_0b
Wire Wire Line
	8700 4050 8800 4050
Text Label 9400 3650 0    50   ~ 0
pwm_1a
Wire Wire Line
	9400 3650 9300 3650
Text Label 9400 3550 0    50   ~ 0
hbt
Wire Wire Line
	9300 3550 9400 3550
Text Label 8700 3550 2    50   ~ 0
ptt
Wire Wire Line
	8700 3550 8800 3550
Text Label 8700 3650 2    50   ~ 0
nav_light
Wire Wire Line
	8700 3650 8800 3650
Text Label 8700 3750 2    50   ~ 0
water_pump_cutoff
Wire Wire Line
	8700 3750 8800 3750
Text Label 8700 3850 2    50   ~ 0
water_sense
Wire Wire Line
	8700 3850 8800 3850
Text Label 8700 3950 2    50   ~ 0
i2c_scl
Text Label 9400 3950 0    50   ~ 0
i2c_sda
Wire Wire Line
	9400 3950 9300 3950
Wire Wire Line
	8800 3950 8700 3950
Text Label 9400 3750 0    50   ~ 0
mag_trigger
Wire Wire Line
	9400 3750 9300 3750
Text Label 6000 4750 0    50   ~ 0
bb_charge_tx
Text Label 6000 4850 0    50   ~ 0
bb_charge_rx
Wire Wire Line
	5900 4750 6000 4750
Wire Wire Line
	5900 4850 6000 4850
$Comp
L Connector_Generic:Conn_01x02 J18
U 1 1 5F6DD510
P 3250 7500
F 0 "J18" V 3122 7580 50  0000 L CNN
F 1 "Conn_01x02" V 3213 7580 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 3250 7500 50  0001 C CNN
F 3 "~" H 3250 7500 50  0001 C CNN
	1    3250 7500
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J16
U 1 1 5F6CD081
P 1150 7500
F 0 "J16" V 1022 7580 50  0000 L CNN
F 1 "Conn_01x02" V 1113 7580 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1150 7500 50  0001 C CNN
F 3 "~" H 1150 7500 50  0001 C CNN
	1    1150 7500
	0    1    1    0   
$EndComp
Text Label 1150 7300 1    50   ~ 0
pwm_0a
Text Label 2200 7300 1    50   ~ 0
pwm_0b
Text Label 3250 7300 1    50   ~ 0
pwm_1a
$Comp
L power:GND #PWR0139
U 1 1 5F6ED208
P 900 7300
F 0 "#PWR0139" H 900 7050 50  0001 C CNN
F 1 "GND" H 905 7127 50  0000 C CNN
F 2 "" H 900 7300 50  0001 C CNN
F 3 "" H 900 7300 50  0001 C CNN
	1    900  7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0140
U 1 1 5F6ED62D
P 1900 7300
F 0 "#PWR0140" H 1900 7050 50  0001 C CNN
F 1 "GND" H 1905 7127 50  0000 C CNN
F 2 "" H 1900 7300 50  0001 C CNN
F 3 "" H 1900 7300 50  0001 C CNN
	1    1900 7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 5F6F69DD
P 3000 7300
F 0 "#PWR0141" H 3000 7050 50  0001 C CNN
F 1 "GND" H 3005 7127 50  0000 C CNN
F 2 "" H 3000 7300 50  0001 C CNN
F 3 "" H 3000 7300 50  0001 C CNN
	1    3000 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 7300 3000 7200
Wire Wire Line
	3000 7200 3150 7200
Wire Wire Line
	3150 7200 3150 7300
Wire Wire Line
	2100 7300 1900 7300
Wire Wire Line
	1050 7300 900  7300
$Comp
L Connector_Generic:Conn_01x02 J17
U 1 1 5F6D7F74
P 2200 7500
F 0 "J17" V 2072 7580 50  0000 L CNN
F 1 "Conn_01x02" V 2163 7580 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 2200 7500 50  0001 C CNN
F 3 "~" H 2200 7500 50  0001 C CNN
	1    2200 7500
	0    1    1    0   
$EndComp
$EndSCHEMATC
